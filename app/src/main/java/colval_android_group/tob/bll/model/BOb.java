package colval_android_group.tob.bll.model;

import java.util.List;

import colval_android_group.tob.util.ContentValue;

public class BOb implements Cloneable {
    final protected long _id;

    public BOb(long id) {
        this._id = id;
    }

    public long getId() {
        return _id;
    }

    @Override
    public String toString() {
        return "BusinessObject{" + "_id=" + _id + '}';
    }

    @Override
    public BOb clone() throws CloneNotSupportedException {
        return (BOb) super.clone();
    }
}
