package colval_android_group.tob.bll.model;

import java.util.ArrayList;
import java.util.List;

import colval_android_group.tob.util.ContentValue;

/**
 * Created by aganm on 11/05/17.
 */

public class Option extends BOb {
    private final String name;
    private final String value;

    public Option (String name, String value) {
        super(0);
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getStringValue() {
        return value;
    }

    public int getIntValue() {
        int ivalue;
        try {
            ivalue = Integer.parseInt(value);
        } catch (Exception e) {
            ivalue = 0;
        }
        return ivalue;
    }

    public float getFloatValue() {
        int fvalue;
        try {
            fvalue = Integer.parseInt(value);
        } catch (Exception e) {
            fvalue = 0;
        }
        return fvalue;
    }
}
