package colval_android_group.tob.bll.model.Pyramid;

import colval_android_group.tob.bll.model.Game;
import colval_android_group.tob.util.Config;

/**
 * Created by 1424842 on 2017-04-21.
 */
public class Pyramid extends Game {
    private int[][] numbers;

    public Pyramid(int width, int height) {
        numbers = new int[width][height];

        seed();
    }

    private void seed() {
        for(int i = 0; i < numbers.length; ++i) {
            for(int j = 0; j < numbers[i].length; ++j){
                numbers[i][j] = i*j;
            }
        }
    }

    public int read(int x, int y) {
        return numbers[x][y];
    }

    public int touch(int x, int y) {
        return (numbers[x][y] *= 2);
    }
}
