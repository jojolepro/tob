package colval_android_group.tob.bll.model;

import java.util.ArrayList;
import java.util.List;

import colval_android_group.tob.util.ContentValue;

/**
 * Created by 1112575 on 2017-04-06.
 */
public class User extends BOb {
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private int score;

    public User(String userName, String password, String firstName, String lastName, String email) {
        super(0);
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.score = 0;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {return password;}

    public void setPassword(String password) { this.password = password;}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() { return firstName + " " + lastName; }

    public int getScore() {return score;}

    public void setScore(int score) {this.score = score;}
}
