package colval_android_group.tob.bll.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import colval_android_group.tob.dto.HighScore;
import colval_android_group.tob.bll.model.User;
import colval_android_group.tob.dal.Registry;
import colval_android_group.tob.util.HighScoreComparator;

/**
 * Created by 1112575 on 2017-04-06.
 */
public class UserService {
    private UserService() {}
    private static User signedInUser;

    public static boolean signIn(String username, String password) {
        User u = Registry.USERS().get(username);
        if (u == null) {
            return false;
        }
        if (u.getPassword().equals(password)) {
            signedInUser = u;
            return true;
        }
        return false;
    }

    public static boolean signOut() {
        if (signedInUser == null) {
            return false;
        }
        signedInUser = null;
        return true;
    }

    public static User getSignedInUser() {
        return signedInUser;
    }

    public static boolean modifyUser(String password, String firstname, String lastname, String email) {
        User mod = new User("", password, firstname, lastname, email);
        return Registry.USERS().set(signedInUser.getUserName(), mod);
    }

    public static boolean modifyOption() {
        return false;
    }

    public static List<HighScore> fetchHighscores() {
        List<HighScore>highScores = new ArrayList();
        for(User user : Registry.USERS().getAll()){
            highScores.add(new HighScore(user.getUserName(), user.getScore()));
        }
        Collections.sort(highScores, new HighScoreComparator());
        return highScores;
    }

    public static boolean signUp(User newU){
        User found = Registry.USERS().get(newU.getUserName());
        if (found != null) {
            return false;
        }
        Registry.USERS().add(newU);
        return true;
    }
}
