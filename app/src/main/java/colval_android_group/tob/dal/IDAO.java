/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colval_android_group.tob.dal;

import java.util.List;

/**
 * @author aganm
 */
public interface IDAO<T> {
    boolean add(T item);

    boolean addAll(List<T> items);

    boolean addAll(T[] items);

    T get(long id);

    List<T> getAll();

    boolean set(T item);

    boolean delete(T item);

    boolean delete(long id);

    void clear();
}
