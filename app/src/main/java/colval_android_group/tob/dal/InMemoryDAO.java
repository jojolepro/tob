package colval_android_group.tob.dal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import colval_android_group.tob.bll.model.BOb;
import colval_android_group.tob.util.BObComparator;

public class InMemoryDAO<T extends BOb> implements IDAO<T> {
    protected final List<T> _BOBS;

    public InMemoryDAO() {
        _BOBS = new ArrayList<T>();
    }

    @Override
    public boolean add(T bob) {
        boolean success = _BOBS.add(bob);
        Collections.sort(_BOBS, new BObComparator());
        return success;
    }

    @Override
    public boolean addAll(List<T> bobs) {
        boolean success = _BOBS.addAll(bobs);
        Collections.sort(_BOBS, new BObComparator());
        return success;
    }

    @Override
    public boolean addAll(T[] bobs) {
        boolean success = _BOBS.addAll(Arrays.asList(bobs));
        Collections.sort(_BOBS, new BObComparator());
        return success;
    }

    @Override
    public T get(long id) {
        int i = Collections.binarySearch(_BOBS, new BOb(id), new BObComparator());
        if (i < 0) {
            return null;
        }
        return _BOBS.get(i);
    }

    @Override
    public List<T> getAll() {
        return _BOBS;
    }

    @Override
    public boolean set(T bob) {
        int i = Collections.binarySearch(_BOBS, bob, new BObComparator());
        if (i < 0) {
            return false;
        }
        _BOBS.set(i, bob);
        return true;
    }

    @Override
    public boolean delete(T bob) {
        return _BOBS.remove(bob);
    }

    @Override
    public boolean delete(long id) {
        int i = Collections.binarySearch(_BOBS, new BOb(id), new BObComparator());
        if (i < 0) {
            return false;
        }
        _BOBS.remove(i);
        return true;
    }

    @Override
    public void clear() {
        _BOBS.clear();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass()).append("{\n");
        for (T t : _BOBS) {
            sb.append("\t").append(t).append(",\n");
        }
        sb.append("}");
        return sb.toString();
    }
}
