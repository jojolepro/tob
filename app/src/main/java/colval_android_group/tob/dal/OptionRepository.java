package colval_android_group.tob.dal;

import colval_android_group.tob.bll.model.Option;
import colval_android_group.tob.bll.model.User;

/**
 * Created by aganm on 11/05/17.
 */

public class OptionRepository extends Repository<Option> {
    public Option get(String name) {
        for(Option o : getAll()) {
            if(o.getName().equals(name)) {
                return o;
            }
        }
        return null;
    }
}
