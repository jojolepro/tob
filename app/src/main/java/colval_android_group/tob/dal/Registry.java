package colval_android_group.tob.dal;

import java.util.ArrayList;
import java.util.List;

import colval_android_group.tob.bll.model.User;

public class Registry {
    private static Registry _instance = new Registry();

    final private UserRepository _userRepository;
    final private OptionRepository _optionRepository;

    private Registry() {
        _userRepository = new UserRepository();
        _optionRepository = new OptionRepository();

        populate();
    }

    public static IDAO newDAO() {
        return new InMemoryDAO();
    }

    private void populate() {
        List<User> users = new ArrayList<>();
        users.add(new User("Bob", "", "Robert", "Gratton", "bob@hotel.ca"));

        _userRepository.addAll(users);
    }

    public static UserRepository USERS() {
        return _instance._userRepository;
    }
    public static OptionRepository OPTIONS() { return _instance._optionRepository; }
}
