package colval_android_group.tob.dal;

import java.util.List;

public class Repository<T> implements IDAO<T> {
    protected IDAO<T> _dao;

    public Repository() {
        _dao = Registry.newDAO();
    }

    @Override
    public boolean add(T bob) {
        return _dao.add(bob);
    }

    @Override
    public boolean addAll(List<T> bobs) {
        return _dao.addAll(bobs);
    }

    @Override
    public boolean addAll(T[] bobs) {
        return _dao.addAll(bobs);
    }

    @Override
    public T get(long id) {
        return _dao.get(id);
    }

    @Override
    public List<T> getAll() {
        return _dao.getAll();
    }

    @Override
    public boolean set(T bob) {
        return _dao.set(bob);
    }

    @Override
    public boolean delete(T bob) {
        return _dao.delete(bob);
    }

    @Override
    public boolean delete(long id) {
        return _dao.delete(id);
    }

    @Override
    public void clear() {
        _dao.clear();
    }

    @Override
    public String toString() {
        return _dao.toString();
    }
}
