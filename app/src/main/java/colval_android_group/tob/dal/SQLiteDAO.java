package colval_android_group.tob.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

import colval_android_group.tob.bll.model.BOb;
import colval_android_group.tob.util.ContentValue;

/**
 * Created by aganm on 11/05/17.
 */

public class SQLiteDAO<T extends BOb> implements IDAO<T> {
    private SQLiteHelper helper;

    public SQLiteDAO(Context context) {
        helper = SQLiteHelper.get(context);
    }

    @Override
    public boolean add(T item) {
        return false;
    }

    @Override
    public boolean addAll(List<T> items) {
        return false;
    }

    @Override
    public boolean addAll(T[] items) {
        return false;
    }

    @Override
    public T get(long id) {
        return null;
    }

    @Override
    public List<T> getAll() {
        return null;
    }

    @Override
    public boolean set(T item) {
        return false;
    }

    @Override
    public boolean delete(T item) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public void clear() {

    }
}
