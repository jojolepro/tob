package colval_android_group.tob.dal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import colval_android_group.tob.util.Config;

/**
 * Created by aganm on 11/05/17.
 */
public class SQLiteHelper extends SQLiteOpenHelper {
    private static SQLiteHelper instance = null;
    private SQLiteDatabase db;

    private SQLiteHelper(Context context) {
        super(context, Config.DB_NAME, null, Config.DB_VERSION);
    }

    public static SQLiteHelper get(Context context) {
        if (instance == null) {
            instance = new SQLiteHelper(context);
        }
        return instance;
    }

    public void open() {
        if (!db.isOpen()) {
            db = this.getWritableDatabase();
        }
    }

    public void close() {
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createConfig = "create table CONFIG (" +
                "ID integer primary key autoincrement," +
                "NAME text not null," +
                "VALUE text not null)";

        String createUser = "create table USER (" +
                "ID integer primary key autoincrement," +
                "USERNAME text not null unique," +
                "PASSWORD text not null," +
                "FIRSTNAME text not null," +
                "LASTNAME text not null," +
                "EMAIL text not null," +
                "SCORE integer not null)";

        db.execSQL(createConfig);
        db.execSQL(createUser);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String dropConfig  = "drop table CONFIG";
        db.execSQL(dropConfig);
        String dropUser  = "drop table USER";
        db.execSQL(dropUser);
        onCreate(db);
    }
}