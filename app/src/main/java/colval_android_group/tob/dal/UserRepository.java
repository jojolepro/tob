package colval_android_group.tob.dal;

import java.util.List;

import colval_android_group.tob.bll.model.User;

/**
 * Created by aganm on 20/04/17.
 */

public class UserRepository extends Repository<User> {
    public User get(String username) {
        for(User u : getAll()) {
            if(u.getUserName().equals(username)) {
                return u;
            }
        }
        return null;
    }

    public boolean set(String username, User mod) {
        User origin = this.get(username);
        origin.setFirstName(mod.getFirstName());
        origin.setLastName(mod.getLastName());
        origin.setEmail(mod.getEmail());
        origin.setPassword(mod.getPassword());
        return true;
    }
}
