package colval_android_group.tob.dto;

/**
 * Created by 1424842 on 2017-04-10.
 */
public class HighScore {
    private String username;
    private int score;

    public HighScore(String username, int score) {
        this.username = username;
        this.score = score;
    }

    public String getUsername() {
        return username;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "HighScore{" +
                "username='" + username + '\'' +
                ", score=" + score +
                '}';
    }
}
