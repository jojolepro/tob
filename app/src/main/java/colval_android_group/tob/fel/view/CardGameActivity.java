package colval_android_group.tob.fel.view;

import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import colval_android_group.tob.R;
import colval_android_group.tob.bll.model.Pyramid.Pyramid;
import colval_android_group.tob.bll.service.UserService;
import colval_android_group.tob.util.ActivityNavigator;
import colval_android_group.tob.util.ChronometerUtil;
import colval_android_group.tob.util.Config;
import colval_android_group.tob.util.HelperAudioPlayer;


public class CardGameActivity extends AppCompatActivity {
    private Button quitButton, pauseButton;
    private Pyramid pyramid;
    private Chronometer timer;
    private Boolean paused;
    private LinearLayout cards1;
    private LinearLayout cards2;
    private ArrayList<Integer> cardsV1;
    private ArrayList<Integer> cardsV2 = new ArrayList<>();
    private final ArrayList<Button> buttonRow1 = new ArrayList<>();
    private final ArrayList<Button> buttonRow2 = new ArrayList<>();
    private long lastPause;

    public static int size = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_game);
        timer = (Chronometer) findViewById(R.id.timer2);
        timer.start();
        paused = false;
        quitButton = (Button)findViewById(R.id.quit2);
        pauseButton = (Button)findViewById(R.id.pause2);
        cards1 = (LinearLayout)findViewById(R.id.cards1);
        cards2 = (LinearLayout)findViewById(R.id.cards2);

        cards1.setOrientation(LinearLayout.HORIZONTAL);
        cards1.setGravity(Gravity.CENTER);
        cards2.setOrientation(LinearLayout.HORIZONTAL);
        cards2.setGravity(Gravity.CENTER);

        cardsV1 = genCardsNumbers(CardGameActivity.size);
        for(int i:cardsV1)
            cardsV2.add(i);
        Collections.shuffle(cardsV2);

        addToLayout(cards1,buttonRow1,cardsV1);
        addToLayout(cards2,buttonRow2,cardsV2);
    }
    public void addToLayout(final LinearLayout l, final ArrayList<Button> backingArr,ArrayList<Integer> n){
        for(final int i:n){
            Button b = new Button(CardGameActivity.this);
            b.setText("");
            b.setLayoutParams(new LinearLayout.LayoutParams(100,110,1));
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //else do nothing, we wait for another user input

                    //if already one selected return; set all row non-clickable
                    //check if other row is selected
                    //if
                    //set both buttons numbers to visible,non clickable
                    //else
                    //show only this one

                    if(isRowSelected(backingArr))return;
                    ((Button)view).setText(String.valueOf(i));
                    for(Button b:backingArr){
                        if(b != view)
                            b.setClickable(false);
                    }

                    if(isRowSelected(getOtherRow(backingArr))){
                        checkMatch();
                    }
                }
            });
            backingArr.add(b);
            l.addView(b);
        }
    }
    public boolean done(){
        for(Button b:buttonRow1)
            if(b.getText().toString().isEmpty())
                return false;
        for(Button b:buttonRow2)
            if(b.getText().toString().isEmpty())
                return false;
        return true;
    }
    public void onReset(View v){
        //view -> action bridge
        reset();
    }
    public void reset(){
        //on reset, turn all to hidden disabled if no text visible && click disabled
        for(Button b:buttonRow1){
            if(b.getText().toString().isEmpty() || b.isClickable()){
                //some repetition here
                b.setClickable(true);
                b.setText("");
            }
        }
        for(Button b:buttonRow2){
            if(b.getText().toString().isEmpty() || b.isClickable()){
                b.setClickable(true);
                b.setText("");
            }
        }
    }
    public void checkMatch(){
        int a = getSelectedIndex(buttonRow1);
        int b = getSelectedIndex(buttonRow2);
        if(cardsV1.get(a).equals(cardsV2.get(b))){
            buttonRow1.get(a).setClickable(false);
            buttonRow2.get(b).setClickable(false);
            reset();

            if(Config.MUSIC_ENABLED)
                HelperAudioPlayer.playSong(this,R.raw.card_game_on_match_found);
            if(done())
                onDone();
        }
    }
    public int getSelectedIndex(ArrayList<Button> arr){
        for(int i=0;i<arr.size();i++){
            Button b = arr.get(i);
            if(b.isClickable() && !b.getText().toString().isEmpty()){
                return i;
            }
        }
        return -1;
    }
    public boolean isRowSelected(ArrayList<Button> bs){
        for(Button b:bs){
            if(b.isClickable() && !b.getText().toString().isEmpty()){
                return true;
            }
        }
        return false;
    }
    public ArrayList<Button> getOtherRow(ArrayList<Button> thisRow){
        if(thisRow == buttonRow1)return buttonRow2;
        return buttonRow1;
    }
    public ArrayList<Integer> genCardsNumbers(int size){
        ArrayList<Integer> out = new ArrayList<>();
        for(int i=1;i<=size;i++){
            out.add(i);
        }
        Collections.shuffle(out);
        return out;
    }
    public void onQuit(View v){
        UserService.getSignedInUser().setScore(ChronometerUtil.getSeconds(timer));
        ActivityNavigator.navigate(this, UserActivity.class);
    }

    public void onPause(View v){
        if(paused){
            timer.setBase(timer.getBase() + SystemClock.elapsedRealtime() - lastPause);
            timer.start();
            paused = false;
            pauseButton.setText("Pause");
        }
        else{
            lastPause = SystemClock.elapsedRealtime();
            timer.stop();
            paused=true;
            pauseButton.setText("Resume");
        }

    }
    public void onDone(){
        int points = (CardGameActivity.size * CardGameActivity.size)-ChronometerUtil.getSeconds(timer);
        Toast.makeText(CardGameActivity.this,"You won "+points+" points!",Toast.LENGTH_LONG).show();
        if(points>0)
            UserService.getSignedInUser().setScore(UserService.getSignedInUser().getScore()+points);
        ActivityNavigator.navigate(this,UserActivity.class);
    }
}
