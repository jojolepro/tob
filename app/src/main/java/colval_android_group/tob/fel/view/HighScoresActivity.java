package colval_android_group.tob.fel.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import colval_android_group.tob.R;
import colval_android_group.tob.dto.HighScore;
import colval_android_group.tob.bll.service.UserService;
import colval_android_group.tob.util.ActivityNavigator;

public class HighScoresActivity extends AppCompatActivity {
    ListView highScoresView;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_scores);
        highScoresView = (ListView) findViewById(R.id.highscores_list_view);
        List<HighScore>highScores = UserService.fetchHighscores();
        List<String> highScoresString = new ArrayList<String>();
        for(HighScore h :  highScores ) {
            highScoresString.add(h.getUsername() + "               " + h.getScore());
        }
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_selectable_list_item
                , highScoresString);
        highScoresView.setAdapter(adapter);

    }

    public void onBackHighScores(View v){
        ActivityNavigator.navigate(this, LoginActivity.class);
    }
}
