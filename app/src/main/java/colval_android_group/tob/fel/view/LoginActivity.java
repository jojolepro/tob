package colval_android_group.tob.fel.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import colval_android_group.tob.R;
import colval_android_group.tob.bll.service.UserService;
import colval_android_group.tob.util.ActivityNavigator;
import colval_android_group.tob.util.PopUp;

public class LoginActivity extends AppCompatActivity {
    EditText username;
    EditText password;
    /*
    Button signIn;
    Button signOut;
    Button manageAccount;
    Button options;
    Button exit;
    Button highscores;
    */
    CheckBox rememberCredentials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        System.out.println("ya boi");

        username = (EditText) findViewById(R.id.login_username);
        password = (EditText) findViewById(R.id.login_password);
        /*
        signIn = (Button)findViewById(R.id.login_sign_in);
        signOut = (Button)findViewById(R.id.login_sign_out);
        manageAccount = (Button)findViewById(R.id.login_manage_account);
        options = (Button)findViewById(R.id.login_options);
        exit = (Button)findViewById(R.id.login_exit);
        highscores = (Button)findViewById(R.id.login_highscores);
        */
        rememberCredentials = (CheckBox) findViewById(R.id.login_remember_credentials);

        username.setText("Bob");
    }

    public void onSignIn(View v) {
        if (UserService.signIn(username.getText().toString(),
                password.getText().toString())) {
            ActivityNavigator.navigate(this, UserActivity.class);
        } else {
            PopUp.showToast(this, "Wrong credentials");
        }
    }

    public void onSignUp(View v) {
        ActivityNavigator.navigate(this, SignupActivity.class);
    }
    public void onHighscores(View v) {
        ActivityNavigator.navigate(this, HighScoresActivity.class);
    }


}
