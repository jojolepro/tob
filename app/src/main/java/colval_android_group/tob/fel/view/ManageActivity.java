package colval_android_group.tob.fel.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import colval_android_group.tob.R;
import colval_android_group.tob.bll.model.User;
import colval_android_group.tob.bll.service.UserService;
import colval_android_group.tob.util.ActivityNavigator;
import colval_android_group.tob.util.Config;
import colval_android_group.tob.util.PopUp;

public class ManageActivity extends AppCompatActivity {

    EditText password;
    EditText confirmPassword;
    EditText firstname;
    EditText lastname;
    EditText email;

    EditText pyramidWidth;
    EditText cardCount;

    CheckBox music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage);
        password = (EditText) findViewById(R.id.manage_password);
        confirmPassword = (EditText) findViewById(R.id.manage_confirmPassword);
        firstname = (EditText) findViewById(R.id.manage_firstname);
        lastname = (EditText) findViewById(R.id.manage_lastname);
        email = (EditText) findViewById(R.id.manage_email);

        pyramidWidth = (EditText) findViewById(R.id.PyramidWidthField);
        cardCount = (EditText) findViewById(R.id.CardCountField);
        music = (CheckBox) findViewById(R.id.music_enabled_field);

        User u = UserService.getSignedInUser();
        firstname.setText(u.getFirstName());
        lastname.setText(u.getLastName());
        email.setText(u.getEmail());

        pyramidWidth.setText(String.valueOf(PyramidActivity.width));
        cardCount.setText(String.valueOf(CardGameActivity.size));

        music.setChecked(Config.MUSIC_ENABLED);
    }

    public void onCommitManage(View v){
        if (!password.getText().toString().equals(confirmPassword.getText().toString())) {
            PopUp.showToast(this, "Passwords don't match");
            return;
        }

        if(!UserService.modifyUser(password.getText().toString(),
                firstname.getText().toString(),
                lastname.getText().toString(), email.getText().toString())) {
            PopUp.showToast(this, "Error modifying user settings.");
            return;
        }

        if(!pyramidWidth.getText().toString().isEmpty()){
            try{
                PyramidActivity.width = Integer.valueOf(pyramidWidth.getText().toString());
            }catch (ClassCastException e){
                PopUp.showToast(this, "Invalid value in Pyramid Width");
            }
        }

        if(!cardCount.getText().toString().isEmpty()){
            try{
                CardGameActivity.size = Integer.valueOf(cardCount.getText().toString());
            }catch (ClassCastException e){
                PopUp.showToast(this, "Invalid value in Card Count");
            }
        }
        Config.MUSIC_ENABLED = music.isChecked();
        PopUp.showToast(this, "Settings saved!");
        ActivityNavigator.navigate(this, UserActivity.class);
    }

    public void onBackManage(View v){
        ActivityNavigator.navigate(this, UserActivity.class);
    }
}
