package colval_android_group.tob.fel.view;

import android.app.Dialog;
import android.graphics.Point;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import colval_android_group.tob.R;
import colval_android_group.tob.util.HelperPyramid;
import colval_android_group.tob.bll.model.Pyramid.Pyramid;
import colval_android_group.tob.bll.service.UserService;
import colval_android_group.tob.util.ActivityNavigator;
import colval_android_group.tob.util.ChronometerUtil;

public class PyramidActivity extends AppCompatActivity {
    private Button quitButton, pauseButton;
    private Pyramid pyramid;
    private Chronometer timer;
    private Boolean paused;
    private LinearLayout grid;
    private ArrayList<ArrayList<Integer>> cases;
    private ArrayList<ArrayList<Button>> buttons;
    private long lastPause;

    public static int width = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pyramid);

        timer = (Chronometer) findViewById(R.id.timer);
        timer.start();
        paused = false;
        quitButton = (Button)findViewById(R.id.rael_quit);
        pauseButton = (Button)findViewById(R.id.rael_pause);

        cases = HelperPyramid.genGrid(width,false);
        Collections.reverse(cases);
        grid = (LinearLayout)findViewById(R.id.grid);
        buttons = new ArrayList<>();
        for(int i=0;i<cases.size();i++){
            ArrayList<Integer> row = cases.get(i);
            LinearLayout r = new LinearLayout(this);
            ArrayList<Button> rb = new ArrayList<>();
            buttons.add(rb);
            r.setOrientation(LinearLayout.HORIZONTAL);
            r.setGravity(Gravity.CENTER);

            for(int j=0;j<row.size();j++){
                int v = row.get(j);
                final Button b = new Button(this);
                rb.add(b);
                if(i == cases.size()-1){
                    b.setText(String.valueOf(v));
                }
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        final Dialog d = new Dialog(PyramidActivity.this);
                        d.setContentView(R.layout.dialog_enter_number);
                        Button confirm = (Button)d.findViewById(R.id.buttonConfirm);
                        confirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view2) {
                                EditText v = (EditText)d.findViewById(R.id.editText);
                                if(v.getText().toString().isEmpty()) return;
                                int in = 0;
                                try{
                                    in = Integer.valueOf(v.getText().toString());
                                }catch(ClassCastException e){
                                    d.dismiss();
                                    Toast.makeText(PyramidActivity.this,"You cannot write a pointed number",Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                Point p = posByButton((Button)view);
                                int val = cases.get(p.x).get(p.y);
                                if(in == val){
                                    b.setText(String.valueOf(in));
                                    Toast.makeText(PyramidActivity.this,"Correct",Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(PyramidActivity.this,"Wrong",Toast.LENGTH_SHORT).show();
                                }
                                checkFullDone();
                                d.dismiss();
                            }
                        });
                        d.show();
                    }
                });
                r.addView(b);
            }

            grid.addView(r);
        }
    }
    public Point posByButton(Button b){
        for(int i = 0;i<buttons.size();i++){
            ArrayList<Button> rb = buttons.get(i);
            for(int j=0;j<rb.size();j++){
                Button b2 = (Button)rb.get(j);
                if(b == b2)return new Point(i,j);
            }
        }
        return null;
    }
    public void checkFullDone(){
        for(int i = 0;i<cases.size();i++){
            ArrayList<Integer> ri = cases.get(i);
            for(int j=0;j<ri.size();j++){
                Button b = buttons.get(i).get(j);
                int v = ri.get(j);
                if(b.getText().toString().isEmpty()) return;
                if(Integer.valueOf(b.getText().toString()) != v) return;
            }
        }
        //Done
        int points = (cases.size() * cases.size() * cases.size())-ChronometerUtil.getSeconds(timer);
        Toast.makeText(PyramidActivity.this,"Done! Points: "+points,Toast.LENGTH_LONG).show();
        if(points>0)
            UserService.getSignedInUser().setScore(UserService.getSignedInUser().getScore()+points);
        ActivityNavigator.navigate(this,UserActivity.class);
    }
    public void onQuit(View v){
        UserService.getSignedInUser().setScore(ChronometerUtil.getSeconds(timer));
        ActivityNavigator.navigate(this, UserActivity.class);
    }

    public void onPause(View v){
        if(paused){
            timer.setBase(timer.getBase() + SystemClock.elapsedRealtime() - lastPause);
            timer.start();
            paused = false;
            pauseButton.setText("Pause");
        }
        else{
            lastPause = SystemClock.elapsedRealtime();
            timer.stop();
            paused=true;
            pauseButton.setText("Resume");
        }

    }
}
