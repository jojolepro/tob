package colval_android_group.tob.fel.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import colval_android_group.tob.R;
import colval_android_group.tob.bll.model.User;
import colval_android_group.tob.bll.service.UserService;
import colval_android_group.tob.util.ActivityNavigator;
import colval_android_group.tob.util.PopUp;

public class SignupActivity extends AppCompatActivity {
    EditText username;
    EditText password;
    EditText firstname;
    EditText lastname;
    EditText email;


    /*
    Button Commit;
    Button Back;
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        username = (EditText) findViewById(R.id.signup_username);
        password = (EditText) findViewById(R.id.signup_password);
        firstname = (EditText) findViewById(R.id.signup_firstname);
        lastname = (EditText) findViewById(R.id.signup_lastname);
        email = (EditText) findViewById(R.id.signup_email);

        /*
        Commit = (Button) findViewById(R.id.signup_commit);
        Back = (Button) findViewById(R.id.signup_back);
        */
    }

    public void onCommit(View v) {
        User u = new User(username.getText().toString(),
                        password.getText().toString(),
                        firstname.getText().toString(),
                        lastname.getText().toString(),
                        email.getText().toString());

        if(username.getText().toString().isEmpty() ||
           password.getText().toString().isEmpty() ||
           firstname.getText().toString().isEmpty() ||
           lastname.getText().toString().isEmpty() ||
           email.getText().toString().isEmpty())
        {
            PopUp.showToast(this, "A field or more are Empty.");
            return;
        }

        if (UserService.signUp(u)) {
            ActivityNavigator.navigate(this, LoginActivity.class);
        } else {
            PopUp.showToast(this, "Wrong information");
        }
    }

    public void onBack(View v) {
        ActivityNavigator.navigate(this, LoginActivity.class);
    }
}
