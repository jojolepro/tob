package colval_android_group.tob.fel.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import colval_android_group.tob.R;
import colval_android_group.tob.bll.model.User;
import colval_android_group.tob.bll.service.UserService;
import colval_android_group.tob.util.ActivityNavigator;


public class UserActivity extends AppCompatActivity {
    TextView username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        username = (TextView) findViewById(R.id.user_username);
        User u = UserService.getSignedInUser();
        username.setText(u.getFullName());
    }

    public void onPlayPyramid(View v){
        ActivityNavigator.navigate(this, PyramidActivity.class);
    }

    public void onGame2(View v){
        ActivityNavigator.navigate(this, CardGameActivity.class);
    }

    public void onGame3(View v){

    }

    public void onManageAccount(View v){
        ActivityNavigator.navigate(this, ManageActivity.class);
    }

    public void onOptions(View v){

    }

    public void onSignOut(View v){
        UserService.signOut();
        ActivityNavigator.navigate(this, LoginActivity.class);
    }


}
