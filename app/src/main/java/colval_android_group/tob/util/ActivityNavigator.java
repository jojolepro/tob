package colval_android_group.tob.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by aganm on 20/04/17.
 */

public class ActivityNavigator {
    private ActivityNavigator() {}

    public static void navigate(Activity from, Class<?> to){
        from.startActivity(new Intent(from, to));
        from.finish();
    }
}
