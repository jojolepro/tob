/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colval_android_group.tob.util;

import java.util.Comparator;

import colval_android_group.tob.bll.model.BOb;

/**
 * @author aganm
 */
public class BObComparator implements Comparator<BOb> {
    @Override
    public int compare(BOb t1, BOb t2) {
        return (int) (t1.getId() - t2.getId());
    }
}
