package colval_android_group.tob.util;

import android.widget.Chronometer;

/**
 * Created by 1424842 on 2017-04-27.
 */
public class ChronometerUtil {
    public static int getSeconds(Chronometer c)
    {
        int stoppedMilliseconds = 0;
        String chronoText = c.getText().toString();
        String array[] = chronoText.split(":");
        if (array.length == 2) {
            stoppedMilliseconds = Integer.parseInt(array[0]) * 60 * 1000
                    + Integer.parseInt(array[1]) * 1000;
        } else if (array.length == 3) {
            stoppedMilliseconds = Integer.parseInt(array[0]) * 60 * 60 * 1000
                    + Integer.parseInt(array[1]) * 60 * 1000
                    + Integer.parseInt(array[2]) * 1000;
        }
        return stoppedMilliseconds / 1000;
    }
}
