package colval_android_group.tob.util;

/**
 * Created by 1112575 on 2017-04-06.
 */
public class Config {
    private Config() {}

    public static final String DB_NAME = "tob.db";
    public static final int DB_VERSION = 1;
    public static boolean MUSIC_ENABLED = true;
}
