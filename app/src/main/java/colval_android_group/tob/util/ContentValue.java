package colval_android_group.tob.util;

/**
 * Created by aganm on 11/05/17.
 */

public class ContentValue<T> {
    private String key;
    private T value;

    public ContentValue(String key, T value)
    {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public T getValue() {
        return value;
    }
}
