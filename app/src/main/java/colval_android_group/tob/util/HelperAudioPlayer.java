package colval_android_group.tob.util;

import android.content.Context;
import android.media.MediaPlayer;

import java.io.IOException;

import colval_android_group.tob.R;

/**
 * Created by jojolepro on 5/15/2017.
 */

public class HelperAudioPlayer {
    public static MediaPlayer playSong(String songPath){
        MediaPlayer p = new MediaPlayer();
        try {
            p.setDataSource(songPath);
            p.prepare();
            p.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p;
    }
    public static MediaPlayer playSong(Context ctx, int resId){
        MediaPlayer p = MediaPlayer.create(ctx,resId);
        p.start();
        return p;
    }
}
