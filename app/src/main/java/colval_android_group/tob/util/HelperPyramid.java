package colval_android_group.tob.util;

import java.util.ArrayList;
import java.util.Random;

public class HelperPyramid {
    public static ArrayList<ArrayList<Integer>> genGrid(int height,boolean doubles){
        Random rand = new Random();
        int h = height;
        ArrayList<ArrayList<Integer>> row = new ArrayList<>();
        while(h > 0){
            ArrayList<Integer> col = new ArrayList<>();
            int w = doubles ? h : h-1;
            if(h == height){
                for(int i=0;i<w;i++){
                    col.add(rand.nextInt(99)+1);
                }
            }else{
                for(int i=0;i<w;i++){
                    ArrayList<Integer> lastCol = row.get(row.size()-1);
                    int lowerLeft = lastCol.get(i);
                    int lowerRight = lastCol.get(i+1);
                    col.add(lowerLeft + lowerRight);
                }
            }
            h -= 1;
            row.add(col);
        }
        return row;
    }
}
