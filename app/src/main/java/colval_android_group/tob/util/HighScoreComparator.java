package colval_android_group.tob.util;

import java.util.Comparator;

import colval_android_group.tob.dto.HighScore;

/**
 * Created by 1112575 on 2017-04-27.
 */
public class HighScoreComparator implements Comparator<HighScore> {

    @Override
    public int compare(HighScore lhs, HighScore rhs) {
        return (lhs.getScore() - rhs.getScore());
    }
}
