package colval_android_group.tob.util;

import java.util.Random;

/**
 * Created by jojolepro on 4/6/17.
 */

public class ObscuredLong {
    private static final String nicememe = "Welcome to the worst anti cheat ever, ma boi";
    private long val;
    private long mask;

    public ObscuredLong(long val) {
        setVal(val);
        Random r = new Random();
        mask = r.nextLong();
    }

    public long getVal() {
        return val ^ mask;
    }

    public void setVal(long val) {
        this.val = val ^ mask;
    }
}
