package colval_android_group.tob.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by aganm on 20/04/17.
 */

public class PopUp {
    private PopUp() {}

    public static void showToast(Activity from, String message) {
        Toast t = Toast.makeText(from.getApplicationContext(), message, Toast.LENGTH_SHORT);
        t.show();
    }

    public static void showDialog(Activity from, String message) throws Exception {
        Dialog d = new Dialog(from.getApplicationContext());
        //d.setContentView();
        d.show();
        throw new Exception();
    }
}
